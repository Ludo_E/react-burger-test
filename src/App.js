import React, { useRef, useEffect } from 'react';
import burger from './burger-menu.svg';
import './App.css';
import './Menu.css';

function App() {
  const darkFilter = "invert(32%) sepia(3%) saturate(947%) hue-rotate(157deg) brightness(94%) contrast(87%)";
  const lightFilter = "invert(100%) sepia(19%) saturate(315%) hue-rotate(252deg) brightness(113%) contrast(95%)";
  const svgBurger = useRef(null);

  useEffect(() => {
    document.addEventListener("scroll", onScrollHandler);
  }, []);

  const onScrollHandler = event => {
    let boundingElementRect = svgBurger.current.getBoundingClientRect();

    let centerX = boundingElementRect.x + boundingElementRect.width / 2;
    let centerY = boundingElementRect.y + boundingElementRect.height / 2;
    let elements = document.elementsFromPoint(centerX, centerY);

    let backgroundElement = elements.find(element => {
      if (element.className.includes("background")) {
        return true;
      } else {
        return false;
      }
    });

    if (backgroundElement) {
      if (backgroundElement.className.includes("dark")) {
        svgBurger.current.style.filter = lightFilter;
      } else  {
        svgBurger.current.style.filter = darkFilter;
      }
    }

  };

  return (
    <div className="App">
      <header>
        <img src={burger} ref={svgBurger} className="burger-menu" alt="menu" />
      </header>
      <main className='main-container'>
        {Array.from({ length: 4 }, (_, i) =>
          <React.Fragment key={i}>
            <section className='dark background'>
              <p>Zone sombre</p>
            </section>
            <section className='light background'>
              <p>Zone claire</p>
            </section>
          </React.Fragment>)}
      </main>
    </div>
  );
}

export default App;
